# Storage
### MySQL
##### Table Structure
Players
```$xslt
CREATE TABLE `core_players` (
  `uuid` mediumtext NOT NULL,
  `joindate` mediumtext NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=38 DEFAULT CHARSET=latin1;
```
Titles
```$xslt
CREATE TABLE `core_titles` (
  `name` varchar(50) NOT NULL,
  `beforeNameTitle` varchar(50) DEFAULT NULL,
  `afterNameTitle` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
```
NameColors
```$xslt
CREATE TABLE `core_namecolors` (
  `color` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
```
ChatColors
```$xslt
CREATE TABLE `core_chatcolors` (
  `color` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
```


