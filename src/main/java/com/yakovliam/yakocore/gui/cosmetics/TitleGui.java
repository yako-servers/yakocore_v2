package com.yakovliam.yakocore.gui.cosmetics;

import com.yakovliam.yakocore.cosmetics.components.Title;
import com.yakovliam.yakocore.cosmetics.utility.CosmeticsUtil;
import com.yakovliam.yakocore.user.User;
import com.yakovliam.yakocoreapi.gui.DynGui;
import com.yakovliam.yakocoreapi.utility.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;

import static com.yakovliam.yakocore.Messages.COSMETICS_TITLE_CLEARED;
import static com.yakovliam.yakocore.Messages.COSMETICS_TITLE_SET;

public class TitleGui extends DynGui {

    private static final ItemStack BACK = new ItemBuilder(Material.ARROW)
            .setDisplayName(ChatColor.GOLD + "Back")
            .build();
    private static final ItemStack NEXT = new ItemBuilder(Material.ARROW)
            .setDisplayName(ChatColor.GOLD + "Next")
            .build();
    private static final ItemStack CLEAR = new ItemBuilder(Material.STAINED_GLASS_PANE)
            .setDisplayName(ChatColor.RED + "Clear")
            .setData(((Integer) 14).byteValue())
            .build();


    public TitleGui(User user, Integer page) {
        super(ChatColor.DARK_GRAY + "Titles - " + page.toString(), 6);

        Collection<Title> titles = CosmeticsUtil.getTitles(user.getUUID());

        Player player = Bukkit.getPlayer(user.getUUID());

        titles.stream()
                .skip((page - 1) * 36)
                .limit(36).forEach(title -> {


            String itemTitle = "";

            if (title.getBeforeNameTitle() != null) itemTitle += title.getBeforeNameTitle();
            itemTitle += "&8" + player.getName();
            if (title.getAfterNameTitle() != null) itemTitle += title.getAfterNameTitle();

            ItemStack item = new ItemBuilder(Material.SIGN).setDisplayName(ChatColor.translateAlternateColorCodes('&', itemTitle)).build();

            String finalItemTitle = itemTitle;
            addItemInteraction(item, (p, ev) -> {
                /* set their color to the color of clicked */

                p.closeInventory(); // close

                COSMETICS_TITLE_SET.msg(p, "%name%", ChatColor.translateAlternateColorCodes('&', finalItemTitle));

                // if they currently don't have a prefix, add the meta key (if they do, remove old and add new)
                Title currentTitle = CosmeticsUtil.getCurrentTitle(user.getUUID());
                if (currentTitle == null) {
                    CosmeticsUtil.addMeta(user.getUUID(), "meta.core_currentTitle." + title.getName());
                } else {
                    CosmeticsUtil.updateMeta(user.getUUID(), "meta.core_currentTitle." + currentTitle.getName(), "meta.core_currentTitle." + title.getName());
                }
            });
        });


        setItemInteraction(45, new ItemBuilder(BACK).build(),
                (p, ev) -> {
                    if (page == 1) {
                        p.closeInventory(); // just close the inventory, since there is no back
                    } else {
                        new TitleGui(user, page - 1).open(p);
                    }
                });

        setItemInteraction(46, new ItemBuilder(CLEAR).build(),
                (p, ev) -> {
                    p.closeInventory(); // close
                    // set the player's current prefix to null
                    Title currentTitle = CosmeticsUtil.getCurrentTitle(user.getUUID());
                    if (currentTitle != null) {
                        CosmeticsUtil.removeCosmeticMeta(user.getUUID(), "Title", currentTitle.getName());
                    }  // fall through
                    COSMETICS_TITLE_CLEARED.msg(p);
                }
        );

        if (titles.size() > page * 36) {
            setItemInteraction(53, new ItemBuilder(NEXT).build(),
                    (p, ev) -> new TitleGui(user, page + 1).open(p));
        }
    }
}
