package com.yakovliam.yakocore.gui.cosmetics;

import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.cosmetics.utility.CosmeticsUtil;
import com.yakovliam.yakocore.user.User;
import com.yakovliam.yakocoreapi.gui.DynGui;
import com.yakovliam.yakocoreapi.utility.ItemBuilder;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;

import static com.yakovliam.yakocore.Messages.COSMETICS_CHATCOLOR_SET;
import static com.yakovliam.yakocore.config.keys.ConfigSettings.CHAT_FORMAT;

public class ChatColorGui extends DynGui {

    public ChatColorGui(User user) {
        super(ChatColor.DARK_GRAY + "Chat Colors", 3);

        // get player
        Player player = Bukkit.getPlayer(user.getUUID());


        String chatFormat = CHAT_FORMAT.get(ConfigSettings.getAdaption());

        chatFormat = chatFormat.replace("%player_displayname%", player.getDisplayName());
        chatFormat = PlaceholderAPI.setPlaceholders(player, chatFormat); // placeholders

        Collection<com.yakovliam.yakocore.cosmetics.components.ChatColor> chatColors = CosmeticsUtil.getChatColors(user.getUUID());

        String finalChatFormat = chatFormat;
        chatColors.forEach(color -> {

            String message = "Hello, how are you?";
            message = ChatColor.translateAlternateColorCodes('&', color.getColor() + message);
            String chatFormatFinal = finalChatFormat.replace("%message%", message);

            ItemStack colorItem = new ItemBuilder(Material.WOOL).setDisplayName(ChatColor.translateAlternateColorCodes('&', chatFormatFinal)).build();

            addItemInteraction(colorItem, (p, ev) -> {
                /* set their color to the color of clicked */

                p.closeInventory(); // close

                COSMETICS_CHATCOLOR_SET.msg(p, "%name%", color.getName());

                // if they currently don't have a color, add the meta key (if they do, remove old and add new)
                com.yakovliam.yakocore.cosmetics.components.ChatColor currentChatColor = CosmeticsUtil.getCurrentChatColor(user.getUUID());

                if (currentChatColor == null) {
                    CosmeticsUtil.addMeta(user.getUUID(), "meta.core_currentChatColor." + color.getName());
                } else {
                    CosmeticsUtil.updateMeta(user.getUUID(), "meta.core_currentChatColor." + currentChatColor.getName(), "meta.core_currentChatColor." + color.getName());
                }
            });
        });
    }
}
