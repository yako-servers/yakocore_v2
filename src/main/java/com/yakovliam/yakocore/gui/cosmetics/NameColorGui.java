package com.yakovliam.yakocore.gui.cosmetics;

import com.yakovliam.yakocore.cosmetics.components.NameColor;
import com.yakovliam.yakocore.cosmetics.utility.CosmeticsUtil;
import com.yakovliam.yakocore.user.User;
import com.yakovliam.yakocoreapi.gui.DynGui;
import com.yakovliam.yakocoreapi.utility.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;

import static com.yakovliam.yakocore.Messages.COSMETICS_NAMECOLOR_SET;

public class NameColorGui extends DynGui {

    public NameColorGui(User user) {
        super(ChatColor.DARK_GRAY + "Name Colors", 3);

        Collection<NameColor> nameColors = CosmeticsUtil.getNameColors(user.getUUID());

        // get player
        Player player = Bukkit.getPlayer(user.getUUID());

        nameColors.forEach(color -> {

            String itemTitle = color.getColor() + player.getName();

            ItemStack colorItem = new ItemBuilder(Material.WOOL).setDisplayName(ChatColor.translateAlternateColorCodes('&', itemTitle)).build();

            addItemInteraction(colorItem, (p, ev) -> {
                /* set their color to the color of clicked */

                p.closeInventory(); // close

                COSMETICS_NAMECOLOR_SET.msg(p, "%name%", color.getName());

                // if they currently don't have a color, add the meta key (if they do, remove old and add new)
                NameColor currentNameColor = CosmeticsUtil.getCurrentNameColor(user.getUUID());
                if (currentNameColor == null) {
                    CosmeticsUtil.addMeta(user.getUUID(), CosmeticsUtil.getCosmeticMetaKey("NameColor", color.getName()));
                } else {
                    CosmeticsUtil.updateMeta(user.getUUID(), CosmeticsUtil.getCosmeticMetaKey("NameColor", currentNameColor.getName()), CosmeticsUtil.getCosmeticMetaKey("NameColor", color.getName()));
                }
            });
        });
    }
}
