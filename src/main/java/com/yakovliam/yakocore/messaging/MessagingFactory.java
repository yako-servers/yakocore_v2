package com.yakovliam.yakocore.messaging;

import com.yakovliam.yakocore.YakoCore;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class MessagingFactory {

    public MessagingFactory() {

    }

    public static void debug(String message) {
        Bukkit.getConsoleSender().sendMessage(ChatColor.GOLD + YakoCore.getInstance().getDescription().getFullName() + " > " + ChatColor.WHITE + message);
    }
}
