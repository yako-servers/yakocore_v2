package com.yakovliam.yakocore.components.clearlag;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.components.TaskComponent;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;

import java.util.Arrays;
import java.util.Iterator;

import static com.yakovliam.yakocore.Messages.CLEARLAG_CLEARED;
import static com.yakovliam.yakocore.Messages.CLEARLAG_LEFT;
import static com.yakovliam.yakocore.config.keys.ConfigSettings.*;

public class ClearLagComponent extends TaskComponent {

    private Integer delaySeconds;
    private Integer secondsLeft;
    private String prefix;
    private boolean enabled;

    @Override
    public void startTask() {
        // load messages and prefix
        enabled = CLEARLAG_ENABLED.get(ConfigSettings.getAdaption());
        prefix = CLEARLAG_PREFIX.get(ConfigSettings.getAdaption());

        delaySeconds = CLEARLAG_DELAY_SECONDS.get(ConfigSettings.getAdaption());
        //delayTicks = delaySeconds.longValue() * 20;

        secondsLeft = delaySeconds;

        // if not enabled, quit!
        if (!enabled) {
            return;
        }

        /* task */

        taskId = Bukkit.getScheduler().runTaskTimerAsynchronously(YakoCore.getInstance(), () -> {

            String format = "seconds";

            if ((secondsLeft % 60 == 0 && !secondsLeft.equals(delaySeconds)) && secondsLeft != 0) { // multiple of 30 (120s, 60s, 30s left) AND not the starting val AND not 0

                int minutesLeft = secondsLeft / 60;
                format = "minutes";

                // broadcast minutes left
                CLEARLAG_LEFT.broadcast("%prefix%", ChatColor.translateAlternateColorCodes('&', prefix), "%format%", format, "%left%", Integer.toString(minutesLeft));

            } else {

                switch (secondsLeft) {
                    case 30:
                        CLEARLAG_LEFT.broadcast("%prefix%", ChatColor.translateAlternateColorCodes('&', prefix), "%format%", format, "%left%", "30");
                        break;
                    case 5:
                        CLEARLAG_LEFT.broadcast("%prefix%", ChatColor.translateAlternateColorCodes('&', prefix), "%format%", format, "%left%", "5");
                        break;
                    case 0:
                        // broadcast
                        CLEARLAG_CLEARED.broadcast("%prefix%", ChatColor.translateAlternateColorCodes('&', prefix));
                        // clear
                        clearLag();
                        // reset secondsLeft
                        secondsLeft = delaySeconds;
                        break;
                }
            }

            secondsLeft--;

        }, 0L, 20L).getTaskId(); // run every second
    }

    private void clearLag() {
        // clear drops in world
        for (World w : Bukkit.getServer().getWorlds()) {
            // catch for CME
            Iterator<Chunk> chunks = Arrays.asList(w.getLoadedChunks()).iterator();
            if (chunks.hasNext()) {
                do {
                    Chunk chunk = chunks.next();
                    for (Entity e : chunk.getEntities()) {
                        if (e instanceof Item) { // if an item
                            e.remove();
                        }
                    }
                } while (chunks.hasNext());
            }
        }
    }
}
