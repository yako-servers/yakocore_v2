package com.yakovliam.yakocore.components.tablist;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.cosmetics.utility.ChatUtil;
import com.yakovliam.yakocore.user.User;
import com.yakovliam.yakocore.user.UserManager;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.TABLIST_FORMAT;

public class TablistComponent implements Listener {

    public TablistComponent() {
        startTablistUpdate();
    }

    private void startTablistUpdate() {
        Bukkit.getScheduler().runTaskTimerAsynchronously(YakoCore.getInstance(), () -> Bukkit.getOnlinePlayers().forEach(this::updateTablistName), 1L, 1200L); // 1 minutes update time
    }

    @EventHandler(priority = EventPriority.HIGH)
    public void onPlayerJoin(PlayerJoinEvent event) {
        updateTablistName(event.getPlayer());
    }

    private void updateTablistName(Player player) {
        User user = UserManager.get(player.getUniqueId());

        String tablistFormat = TABLIST_FORMAT.get(ConfigSettings.getAdaption());

        String displayName = ChatUtil.getDisplayNameFormat(user);

        tablistFormat = tablistFormat.replace("%player_displayname%", displayName);
        tablistFormat = PlaceholderAPI.setPlaceholders(player, tablistFormat); // placeholders

        player.setPlayerListName(tablistFormat);

    }
}
