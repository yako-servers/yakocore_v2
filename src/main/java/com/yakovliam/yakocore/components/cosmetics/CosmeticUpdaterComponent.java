package com.yakovliam.yakocore.components.cosmetics;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.components.TaskComponent;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.cosmetics.components.ChatColor;
import com.yakovliam.yakocore.cosmetics.components.NameColor;
import com.yakovliam.yakocore.cosmetics.components.Title;
import com.yakovliam.yakocore.cosmetics.utility.CosmeticsUtil;
import com.yakovliam.yakocore.permission.LuckPermsAdapter;
import com.yakovliam.yakocore.user.User;
import com.yakovliam.yakocore.user.UserManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.Collection;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.COSMETICS_UPDATE_PERIOD_TICKS;

public class CosmeticUpdaterComponent extends TaskComponent {

    @Override
    public void startTask() {
        // load messages and prefix
        int updatePeriod = COSMETICS_UPDATE_PERIOD_TICKS.get(ConfigSettings.getAdaption());

        /* task */

        taskId = Bukkit.getScheduler().runTaskTimerAsynchronously(YakoCore.getInstance(), () -> {

            //----------------------------------------------------

            // loop through all players, see if they have it
            Collection<? extends Player> playerList = Bukkit.getOnlinePlayers();

            playerList.forEach(player -> {

                if (!player.isOnline()) {
                    return;
                }

                User user = UserManager.get(player.getUniqueId());

                if (user == null) return;

                // TITLE -------------------------------------------------------
                Title currentTitle = CosmeticsUtil.getCurrentTitle(user.getUUID());
                if (currentTitle != null) {
                    // if their current prefix is one of these
                    if (!LuckPermsAdapter.hasPermission(player.getUniqueId(), CosmeticsUtil.getPermissionNodeFromCosmetic("title", currentTitle.getName()))) {
                        // set defaults, since they don't have permission
                        CosmeticsUtil.removeCosmeticMeta(user.getUUID(), "Title", currentTitle.getName());
                    }
                }

                // NAME COLOR ---------------------------------------------------
                NameColor currentNameColor = CosmeticsUtil.getCurrentNameColor(user.getUUID());
                if (currentNameColor != null) {
                    if (!LuckPermsAdapter.hasPermission(player.getUniqueId(), CosmeticsUtil.getPermissionNodeFromCosmetic("namecolor", currentNameColor.getName()))) {
                        CosmeticsUtil.removeCosmeticMeta(user.getUUID(), "NameColor", currentNameColor.getName());
                    }
                }

                // CHAT COLOR ---------------------------------------------------
                ChatColor currentChatColor = CosmeticsUtil.getCurrentChatColor(user.getUUID());
                if (currentChatColor != null) {
                    if (!LuckPermsAdapter.hasPermission(player.getUniqueId(), CosmeticsUtil.getPermissionNodeFromCosmetic("chatcolor", currentChatColor.getName()))) {
                        CosmeticsUtil.removeCosmeticMeta(user.getUUID(), "ChatColor", currentChatColor.getName());
                    }
                }

            });
        }, 0L, updatePeriod).getTaskId(); // run every x seconds (probably)
    }
}
