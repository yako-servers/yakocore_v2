package com.yakovliam.yakocore.components.broadcast;

import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocoreapi.chat.Message;
import org.bukkit.ChatColor;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.BROADCAST_PREFIX;

public class BroadcastComponent {
    private String prefix;

    public BroadcastComponent() {
        // load messages and prefix
        prefix = BROADCAST_PREFIX.get(ConfigSettings.getAdaption());
    }

    public void broadcast(String message) {
        String prefixWithMessage = ChatColor.translateAlternateColorCodes('&', prefix + message);

        Message broadcastMessage = new Message("broadcast-message-temp", prefixWithMessage);
        broadcastMessage.broadcast();
    }
}
