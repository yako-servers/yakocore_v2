package com.yakovliam.yakocore.components.broadcast;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.components.TaskComponent;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

import java.util.List;
import java.util.Random;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.*;

public class AutoBroadcastComponent extends TaskComponent {

    private List<String> messages;
    private String prefix;
    private boolean enabled;
    private Long delayTicks;

    @Override
    public void startTask() {
        // load messages and prefix

        enabled = BROADCAST_AUTO_ENABLED.get(ConfigSettings.getAdaption());

        prefix = BROADCAST_AUTO_PREFIX.get(ConfigSettings.getAdaption());

        messages = BROADCAST_AUTO_MESSAGES.get(ConfigSettings.getAdaption());

        if (messages == null)
            return;

        Integer delaySeconds = BROADCAST_AUTO_DELAY_SECONDS.get(ConfigSettings.getAdaption());

        delayTicks = delaySeconds.longValue() * 20;

        // if not enabled, quit!
        if (!enabled) {
            return;
        }

        /* task */

        taskId = Bukkit.getScheduler().runTaskTimerAsynchronously(YakoCore.getInstance(), () -> {

            // pick random message
            String message = getRandomMessage();

            String broadcastMessage = ChatColor.translateAlternateColorCodes('&', message.replace("%prefix%", prefix));

            Bukkit.getOnlinePlayers().forEach(player -> {
                // replace placeholders
                String placeholderBroadcastMessage = PlaceholderAPI.setPlaceholders(player, broadcastMessage);
                player.sendMessage(placeholderBroadcastMessage);
            });

        }, 0L, delayTicks).getTaskId();

    }

    private String getRandomMessage() {
        Random rand = new Random();
        return messages.get(rand.nextInt(messages.size()));
    }
}
