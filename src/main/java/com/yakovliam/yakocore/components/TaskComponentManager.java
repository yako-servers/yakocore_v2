package com.yakovliam.yakocore.components;

import com.yakovliam.yakocore.components.broadcast.AutoBroadcastComponent;
import com.yakovliam.yakocore.components.broadcast.BroadcastComponent;
import com.yakovliam.yakocore.components.clearlag.ClearLagComponent;
import com.yakovliam.yakocore.components.cosmetics.CosmeticUpdaterComponent;

import java.util.ArrayList;
import java.util.List;

@Deprecated
public class TaskComponentManager {

    private static BroadcastComponent broadcastComponent;
    public static BroadcastComponent getBroadcastComponent() {
        return broadcastComponent;
    }

    public static List<TaskComponent> toggleableTaskComponents = new ArrayList<>();

    public static void registerFeature(TaskComponent taskComponent) {
        toggleableTaskComponents.add(taskComponent);

        // start feature
        taskComponent.startTask();
    }

    public TaskComponentManager() {
        broadcastComponent = new BroadcastComponent(); // reg broadcast separately, since it's not a toggleable

        // register features
        registerFeature(new AutoBroadcastComponent());
        registerFeature(new ClearLagComponent());
        registerFeature(new CosmeticUpdaterComponent());
    }

    public static void reloadFeatures() {
        // stop all
        toggleableTaskComponents.forEach(TaskComponent::stopTask);

        // start all again
        toggleableTaskComponents.forEach(TaskComponent::startTask);
    }

}
