package com.yakovliam.yakocore.components;

import org.bukkit.Bukkit;

@Deprecated
public abstract class TaskComponent {
    public int taskId;

    public void stopTask() {
        try {
            Bukkit.getScheduler().cancelTask(taskId);
        } catch (Exception ignored) {
        }
    }

    public abstract void startTask();

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public boolean isEnabled() {
        return Bukkit.getScheduler().isCurrentlyRunning(taskId);
    }
}
