package com.yakovliam.yakocore.context;

public class ContextResult {

    private String message;
    private Boolean result;

    public ContextResult(String message, Boolean result) {
        this.message = message;
        this.result = result;
    }

    public ContextResult() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }
}
