package com.yakovliam.yakocore.papi;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.cosmetics.components.ChatColor;
import com.yakovliam.yakocore.cosmetics.components.NameColor;
import com.yakovliam.yakocore.cosmetics.components.Title;
import com.yakovliam.yakocore.cosmetics.utility.CosmeticsUtil;
import me.clip.placeholderapi.expansion.PlaceholderExpansion;
import org.bukkit.entity.Player;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.COSMETICS_DEFAULT_CHATCOLOR;
import static com.yakovliam.yakocore.config.keys.ConfigSettings.COSMETICS_DEFAULT_NAMECOLOR;

/**
 * This class will automatically register as a placeholder expansion
 * when a jar including this class is added to the /plugins/placeholderapi/expansions/ folder
 */
public class PapiExpansion extends PlaceholderExpansion {

    /**
     * This method should always return true unless we
     * have a dependency we need to make sure is on the server
     * for our placeholders to work!
     * This expansion does not require a dependency so we will always return true
     */
    @Override
    public boolean canRegister() {
        return true;
    }

    /**
     * The name of the person who created this expansion should go here
     */
    @Override
    public String getAuthor() {
        return "yakovliam";
    }

    /**
     * The placeholder identifier should go here
     * This is what tells PlaceholderAPI to call our onPlaceholderRequest method to obtain
     * a value if a placeholder starts with our identifier.
     * This must be unique and can not contain % or _
     */
    @Override
    public String getIdentifier() {
        return "core";
    }

    /**
     * This is the version of this expansion
     */
    @Override
    public String getVersion() {
        return YakoCore.getInstance().getDescription().getVersion();
    }

    /**
     * This is the method called when a placeholder with our identifier is found and needs a value
     * We specify the value identifier in this method
     */
    @Override
    public String onPlaceholderRequest(Player p, String identifier) {

        if (!p.isOnline()) return null;

        /* ------------------------------- VALUES --------------------------------*/

        if (identifier.equalsIgnoreCase("prefix")) {
            Title title = CosmeticsUtil.getCurrentTitle(p.getUniqueId());

            if (title != null && title.getBeforeNameTitle() != null)
                return title.getBeforeNameTitle();
            else
                return "";
        }

        if (identifier.equalsIgnoreCase("suffix")) {
            Title title = CosmeticsUtil.getCurrentTitle(p.getUniqueId());

            if (title != null && title.getAfterNameTitle() != null)
                return title.getAfterNameTitle();
            else
                return "";
        }

        if (identifier.equalsIgnoreCase("namecolor")) {
            NameColor color = CosmeticsUtil.getCurrentNameColor(p.getUniqueId());

            if (color != null && color.getColor() != null)
                return color.getColor();
            else
                // return DEFAULT name color
                return COSMETICS_DEFAULT_NAMECOLOR.get(ConfigSettings.getAdaption());
        }

        if (identifier.equalsIgnoreCase("chatcolor")) {
            ChatColor color = CosmeticsUtil.getCurrentChatColor(p.getUniqueId());

            if (color != null && color.getColor() != null)
                return color.getColor();
            else
                // return DEFAULT chat color
                return COSMETICS_DEFAULT_CHATCOLOR.get(ConfigSettings.getAdaption());
        }

        /* ------------------------------- NAMES --------------------------------*/

        if (identifier.equalsIgnoreCase("titlename")) {
            Title title = CosmeticsUtil.getCurrentTitle(p.getUniqueId());

            if (title != null && title.getName() != null)
                return title.getName();
            else
                return "None";
        }

        if (identifier.equalsIgnoreCase("namecolorname")) {
            NameColor color = CosmeticsUtil.getCurrentNameColor(p.getUniqueId());

            if (color != null && color.getName() != null)
                return color.getName();
            else
                return "None";
        }

        if (identifier.equalsIgnoreCase("chatcolorname")) {
            ChatColor color = CosmeticsUtil.getCurrentChatColor(p.getUniqueId());

            if (color != null && color.getName() != null)
                return color.getName();
            else
                return "None";
        }

        return null;
    }
}

