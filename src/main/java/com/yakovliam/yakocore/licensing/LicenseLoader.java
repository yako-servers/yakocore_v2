package com.yakovliam.yakocore.licensing;

import com.yakovliam.yakocore.config.keys.LicensingSettings;
import com.yakovliam.yakocore.context.ContextResult;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

import static com.yakovliam.yakocore.config.keys.LicensingSettings.*;

public class LicenseLoader {

    public void load() {

        // get params
        String url = POST_URL.get(LicensingSettings.getAdaption());
        String authId = AUTH_ID.get(LicensingSettings.getAdaption());
        String authToken = AUTH_TOKEN.get(LicensingSettings.getAdaption());

        // post to url
        ContextResult result = sendPOST(url, authId, authToken);
        if (!result.getResult()) {
            // print message
            Bukkit.getConsoleSender().sendMessage(ChatColor.RED + "LICENSE ERROR > " + ChatColor.WHITE + result.getMessage() + ChatColor.RED + " - SHUTTING DOWN NOW");
            // exit immediately
            System.exit(0);
        }
    }

    private ContextResult sendPOST(String url, String authId, String authToken) {

        try {
            Map<String, String> params = new HashMap<>();
            params.put("authId", authId);
            params.put("authToken", authToken);

            org.json.simple.JSONObject paramsJSON = new org.json.simple.JSONObject(params);


            StringEntity entity = new StringEntity(paramsJSON.toString(),
                    ContentType.APPLICATION_JSON);

            HttpClient httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(url);
            request.setEntity(entity);

            HttpResponse response = httpClient.execute(request);


            StringBuilder textBuilder = new StringBuilder();
            try (Reader reader = new BufferedReader(new InputStreamReader
                    (response.getEntity().getContent(), StandardCharsets.UTF_8))) {
                int c;
                while ((c = reader.read()) != -1) {
                    textBuilder.append((char) c);
                }
            }

            // print result
            JSONObject responseJSON = new JSONObject(textBuilder.toString());

            boolean authorized = responseJSON.getBoolean("accept");
            if (!authorized)
                return new ContextResult("Not authorized", false);

        } catch (Exception e) {
            return new ContextResult(e.getMessage(), false);
        }

        return new ContextResult("No Errors", true);
    }

}
