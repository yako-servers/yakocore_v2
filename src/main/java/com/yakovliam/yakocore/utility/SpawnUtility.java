package com.yakovliam.yakocore.utility;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.config.SpawnConfig;
import com.yakovliam.yakocoreapi.YakoCoreAPI;
import jdk.vm.ci.meta.Local;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.UUID;

@Deprecated
public class SpawnUtility {

    public static void setSpawnLocation(Player player) {
        Location spawnLocation = player.getLocation();

        SpawnConfig config = YakoCore.getInstance().getSpawnConfig();

        config.set("spawn", spawnLocation);
    }

    public static Location getSpawnLocation() {
        SpawnConfig config = YakoCore.getInstance().getSpawnConfig();
        if (config.getConfig().get("spawn") == null || config.getConfig().get("spawn") == "")
            return null;

        Location spawnLocation = null;

        try {
            spawnLocation = (Location) config.getConfig().get("spawn");
        } catch (Exception e) {
            YakoCoreAPI.err("Error when teleporting user to spawn: " + e.getMessage());
        }

        return spawnLocation;
    }

    public static void teleportToSpawn(Player player) {
        Location spawnLocation = getSpawnLocation();

        if (spawnLocation == null)
            return;

        player.teleport(spawnLocation);
    }

    public static Integer getPlayerCount() {
        return YakoCore.getInstance().getStorageManager().getStorage().getPlayerCount();
    }

    public static boolean isPlayerNew(UUID uuid) {
        return YakoCore.getInstance().getStorageManager().getStorage().isPlayerNew(uuid);
    }
}
