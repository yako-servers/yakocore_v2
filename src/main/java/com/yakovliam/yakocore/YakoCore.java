package com.yakovliam.yakocore;

import com.yakovliam.yakocore.chat.ChatMiddleWareManager;
import com.yakovliam.yakocore.command.loader.CommandLoader;
import com.yakovliam.yakocore.components.TaskComponentManager;
import com.yakovliam.yakocore.config.*;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.context.ContextResult;
import com.yakovliam.yakocore.cosmetics.ChatColorManager;
import com.yakovliam.yakocore.cosmetics.NameColorManager;
import com.yakovliam.yakocore.cosmetics.TitleManager;
import com.yakovliam.yakocore.licensing.LicenseLoader;
import com.yakovliam.yakocore.listener.PlayerChatListener;
import com.yakovliam.yakocore.listener.PlayerJoinQuitListener;
import com.yakovliam.yakocore.listener.PlayerKickListener;
import com.yakovliam.yakocore.messaging.MessagingFactory;
import com.yakovliam.yakocore.papi.PapiExpansion;
import com.yakovliam.yakocore.permission.LuckPermsAPI;
import com.yakovliam.yakocore.storage.StorageManager;
import com.yakovliam.yakocoreapi.YakoCoreAPI;
import com.yakovliam.yakocoreapi.YakoPlugin;

import java.util.concurrent.CompletableFuture;

public class YakoCore extends YakoPlugin {

    private static YakoCore instance;
    private YakoCoreConfig coreConfig;
    private CommandLoader commandLoader;
    private StorageManager storageManager;
    private StorageConfig storageConfig;
    private TitleManager titleManager;
    private NameColorManager nameColorManager;
    private ChatColorManager chatColorManager;
    private RulesConfig rulesConfig;
    private SpawnConfig spawnConfig;
    private LicensingConfig licensingConfig;
    private MessagesConfig messagesConfig;
    private Messages messages;

    public MessagesConfig getMessagesConfig() {
        return messagesConfig;
    }

    public SpawnConfig getSpawnConfig() {
        return spawnConfig;
    }

    public CommandLoader getCommandLoader() {
        return commandLoader;
    }

    public RulesConfig getRulesConfig() {
        return rulesConfig;
    }

    public TitleManager getTitleManager() {
        return titleManager;
    }

    public NameColorManager getNameColorManager() {
        return nameColorManager;
    }

    public ChatColorManager getChatColorManager() {
        return chatColorManager;
    }

    public StorageManager getStorageManager() {
        return storageManager;
    }

    public YakoCoreConfig getCoreConfig() {
        return coreConfig;
    }

    public StorageConfig getStorageConfig() {
        return storageConfig;
    }

    public static YakoCore getInstance() {
        return instance;
    }

    public LicensingConfig getLicensingConfig() {
        return licensingConfig;
    }

    public void setCoreConfig(YakoCoreConfig coreConfig) {
        this.coreConfig = coreConfig;
    }

    public Messages getMessages() {
        return messages;
    }

    @Override
    public void onLoad() {
        instance = this;
    }


    @Override
    public void onEnable() {
        // Plugin enable logic

        initAPIs();

        // listeners
        registerListeners(new PlayerChatListener(), new PlayerJoinQuitListener(), new PlayerKickListener());

        // configs
        coreConfig = new YakoCoreConfig();
        storageConfig = new StorageConfig();
        rulesConfig = new RulesConfig();
        spawnConfig = new SpawnConfig();
        messagesConfig = new MessagesConfig();
        licensingConfig = new LicensingConfig();

        // init storage
        storageManager = new StorageManager();

        // load commands
        commandLoader = new CommandLoader();

        // messages
        messages = new Messages();

        CompletableFuture<ContextResult> loadResult = commandLoader.load();
        if (ConfigSettings.DEBUG.get(ConfigSettings.getAdaption()))
            MessagingFactory.debug(loadResult.getNow(null).getMessage());

        // cosmetics
        titleManager = new TitleManager();
        nameColorManager = new NameColorManager();
        chatColorManager = new ChatColorManager();

        // load all components
        new TaskComponentManager();

        // load middleWares for chat
        new ChatMiddleWareManager();

        // init papi expansion
        new PapiExpansion().register();

        // load license
        new LicenseLoader().load();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    private void initAPIs() {
        YakoCoreAPI.getInstance(this); // core api
        new LuckPermsAPI(); // lp api
    }

    private void reloadComponents() {
        YakoCore.getInstance().getCoreConfig().load();
        YakoCore.getInstance().getMessagesConfig().load();
        TaskComponentManager.reloadFeatures(); // task components
        messages = new Messages();
    }
}
