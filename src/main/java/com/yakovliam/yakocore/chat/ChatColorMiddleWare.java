package com.yakovliam.yakocore.chat;

import com.yakovliam.yakocore.permission.LuckPermsAdapter;
import com.yakovliam.yakocore.user.User;
import org.bukkit.ChatColor;

public class ChatColorMiddleWare implements ChatMiddleWare {

    @Override
    public String parse(User user, String message) {
        // parse chat colors
        if (!LuckPermsAdapter.hasPermission(user.getUUID(), "yakocore.chatcolors")) {
            message = ChatColor.stripColor(ChatColor.translateAlternateColorCodes('&', message)); // no, strip colors
        }
        return message;
    }
}
