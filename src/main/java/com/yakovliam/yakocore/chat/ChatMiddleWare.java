package com.yakovliam.yakocore.chat;

import com.yakovliam.yakocore.user.User;

public interface ChatMiddleWare {
    String parse(User user, String message);
}
