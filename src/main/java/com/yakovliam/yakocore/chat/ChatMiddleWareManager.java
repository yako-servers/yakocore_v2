package com.yakovliam.yakocore.chat;

import com.yakovliam.yakocore.user.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChatMiddleWareManager {

    private static List<ChatMiddleWare> middleWareList;

    public ChatMiddleWareManager() {
        middleWareList = new ArrayList<>();

        middleWareList.addAll(Collections.singletonList(
                new ChatColorMiddleWare()
        ));
    }

    public static String apply(User user, String message) {
        for (ChatMiddleWare mW : middleWareList) {
            message = mW.parse(user, message);
        }

        return message;
    }
}
