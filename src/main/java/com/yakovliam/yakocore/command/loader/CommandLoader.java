package com.yakovliam.yakocore.command.loader;

import com.yakovliam.yakocore.commands.YakoCoreCommand;
import com.yakovliam.yakocore.commands.blocking.PluginsCommand;
import com.yakovliam.yakocore.commands.broadcast.BroadcastCommand;
import com.yakovliam.yakocore.commands.broadcast.StartBroadcastCommand;
import com.yakovliam.yakocore.commands.broadcast.StopBroadcastCommand;
import com.yakovliam.yakocore.commands.cosmetics.ChatColorCommand;
import com.yakovliam.yakocore.commands.cosmetics.NameColorCommand;
import com.yakovliam.yakocore.commands.cosmetics.TitleCommand;
import com.yakovliam.yakocore.commands.dep.RulesCommand;
import com.yakovliam.yakocore.commands.spawn.SetSpawnCommand;
import com.yakovliam.yakocore.commands.spawn.SpawnCommand;
import com.yakovliam.yakocore.context.ContextResult;

import java.util.concurrent.CompletableFuture;

public class CommandLoader {

    public CompletableFuture<ContextResult> load() {
        ContextResult result = new ContextResult();

        try {
            // load commands
            new YakoCoreCommand();

            new PluginsCommand();

            new BroadcastCommand();
            new StartBroadcastCommand();
            new StopBroadcastCommand();

            new ChatColorCommand();
            new TitleCommand();
            new NameColorCommand();

            new RulesCommand();

            new SpawnCommand();
            new SetSpawnCommand();

            result.setResult(true);
            result.setMessage("Commands load success");
        } catch (Exception e) {
            result.setResult(false);
            result.setMessage("Commands load failure");
        }

        CompletableFuture<ContextResult> futureResult = new CompletableFuture<>();
        futureResult.complete(result);

        return futureResult;
    }
}
