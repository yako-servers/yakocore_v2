package com.yakovliam.yakocore.user;

import java.util.Date;
import java.util.UUID;

public class User {

    private UUID uuid;
    private Date joinDate;

    public User(UUID uuid) {
        this.uuid = uuid;
    }

    public User(UUID uuid, Date joinDate) {
        this.uuid = uuid;
        this.joinDate = joinDate;
    }

    public UUID getUUID() {
        return uuid;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }
}
