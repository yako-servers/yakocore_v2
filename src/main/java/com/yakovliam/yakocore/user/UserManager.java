package com.yakovliam.yakocore.user;

import com.github.benmanes.caffeine.cache.AsyncLoadingCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.yakovliam.yakocore.YakoCore;
import org.bukkit.Bukkit;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class UserManager {
    @SuppressWarnings("all")
    private static AsyncLoadingCache<UUID, User> userCache = Caffeine.newBuilder()
            .refreshAfterWrite(5, TimeUnit.MINUTES) /* refresh after 5 MINUTES */
            .buildAsync(key -> {

                // get user
                User user = YakoCore.getInstance().getStorageManager().getStorage().getUser(key);

                if (user == null) {
                    YakoCore.getInstance().getStorageManager().getStorage().insertUser(new User(key, new Date()));
                }

                return user; // done! return the finished user object
            });

    public static User get(UUID uuid) {
        try {
            return userCache.get(uuid).get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void invalidate(UUID uuid) {
        userCache.asMap().remove(uuid);
    }

    public static void reValidate(UUID uuid) {
        Bukkit.getScheduler().runTaskAsynchronously(YakoCore.getInstance(), () -> { // async
            invalidate(uuid);
            get(uuid);
        });
    }

    public static void localPersist(User user) {
        CompletableFuture<User> completableFutureUser = CompletableFuture.completedFuture(user);

        userCache.put(user.getUUID(), completableFutureUser);
    }
}
