package com.yakovliam.yakocore.config;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.Config;

public class MessagesConfig extends Config {

    public MessagesConfig() {
        super(YakoCore.getInstance(), "messages.yml");
    }
}
