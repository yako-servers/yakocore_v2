package com.yakovliam.yakocore.config;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.Config;

public class StorageConfig extends Config {

    public StorageConfig() {
        super(YakoCore.getInstance(), "storage.yml");
    }
}
