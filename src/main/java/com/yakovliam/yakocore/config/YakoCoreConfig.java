package com.yakovliam.yakocore.config;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.Config;

public class YakoCoreConfig extends Config {

    public YakoCoreConfig() {
        super(YakoCore.getInstance(), "config.yml");
    }
}
