package com.yakovliam.yakocore.config;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.Config;

public class SpawnConfig extends Config {

    public SpawnConfig() {
        super(YakoCore.getInstance(), "spawn.yml");
    }
}
