package com.yakovliam.yakocore.config.keys;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.ConfigKey;
import com.yakovliam.yakocoreapi.config.ConfigKeyTypes;
import com.yakovliam.yakocoreapi.config.adapter.BukkitConfigurateAdaption;

public class StorageSettings {

    public static final ConfigKey<String> MYSQL_ADDRESS = ConfigKeyTypes.stringKey("mysql.address", null);
    public static final ConfigKey<Integer> MYSQL_PORT = ConfigKeyTypes.integerKey("mysql.port", 3306);
    public static final ConfigKey<String> MYSQL_USERNAME = ConfigKeyTypes.stringKey("mysql.username", null);
    public static final ConfigKey<String> MYSQL_PASSWORD = ConfigKeyTypes.stringKey("mysql.password", null);
    public static final ConfigKey<String> MYSQL_DATABASE = ConfigKeyTypes.stringKey("mysql.database", null);

    public static final ConfigKey<String> MYSQL_TABLES_TITLES = ConfigKeyTypes.stringKey("mysql.tables.titles", null);
    public static final ConfigKey<String> MYSQL_TABLES_NAMECOLORS = ConfigKeyTypes.stringKey("mysql.tables.namecolors", null);
    public static final ConfigKey<String> MYSQL_TABLES_CHATCOLORS = ConfigKeyTypes.stringKey("mysql.tables.chatcolors", null);
    public static final ConfigKey<String> MYSQL_TABLES_PLAYERS = ConfigKeyTypes.stringKey("mysql.tables.players", null);

    public static final ConfigKey<Integer> HIKARI_MAX_POOL_SIZE = ConfigKeyTypes.integerKey("hikari.maxPoolSize", 100);
    public static final ConfigKey<Integer> HIKARI_SET_IDLE_TIMEOUT = ConfigKeyTypes.integerKey("hikari.setIdleTimeout", 30000);


    public static BukkitConfigurateAdaption getAdaption() {
        return new BukkitConfigurateAdaption(YakoCore.getInstance(), YakoCore.getInstance().getStorageConfig());
    }
}
