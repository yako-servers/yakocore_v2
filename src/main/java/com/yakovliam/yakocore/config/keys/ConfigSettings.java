package com.yakovliam.yakocore.config.keys;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.ConfigKey;
import com.yakovliam.yakocoreapi.config.ConfigKeyTypes;
import com.yakovliam.yakocoreapi.config.adapter.BukkitConfigurateAdaption;

import java.util.ArrayList;
import java.util.List;

public class ConfigSettings {

    public static final ConfigKey<Boolean> DEBUG = ConfigKeyTypes.booleanKey("debug", false);

    public static final ConfigKey<Boolean> SPAWN_TELEPORT_ON_JOIN = ConfigKeyTypes.booleanKey("spawn-teleport-on-join", false);

    public static final ConfigKey<String> COSMETICS_DISPLAYNAME_FORMAT = ConfigKeyTypes.stringKey("cosmetics.displayname-format", null);
    public static final ConfigKey<String> COSMETICS_DEFAULT_NAMECOLOR = ConfigKeyTypes.stringKey("cosmetics.default-namecolor", null);
    public static final ConfigKey<String> COSMETICS_DEFAULT_CHATCOLOR = ConfigKeyTypes.stringKey("cosmetics.default-chatcolor", null);
    public static final ConfigKey<Integer> COSMETICS_UPDATE_PERIOD_TICKS = ConfigKeyTypes.integerKey("cosmetics.update-period-ticks", 2400);

    public static final ConfigKey<String> BROADCAST_PREFIX = ConfigKeyTypes.stringKey("broadcast.prefix", null);

    public static final ConfigKey<Boolean> BROADCAST_AUTO_ENABLED = ConfigKeyTypes.booleanKey("broadcast.auto.enabled", false);
    public static final ConfigKey<String> BROADCAST_AUTO_PREFIX = ConfigKeyTypes.stringKey("broadcast.auto.prefix", null);
    public static final ConfigKey<List<String>> BROADCAST_AUTO_MESSAGES = ConfigKeyTypes.stringListKey("broadcast.auto.messages", new ArrayList<>());
    public static final ConfigKey<Integer> BROADCAST_AUTO_DELAY_SECONDS = ConfigKeyTypes.integerKey("broadcast.auto.delay-seconds", 60);

    public static final ConfigKey<String> CLEARLAG_PREFIX = ConfigKeyTypes.stringKey("clear-lag.prefix", null);
    public static final ConfigKey<Boolean> CLEARLAG_ENABLED = ConfigKeyTypes.booleanKey("clear-lag.enabled", false);
    public static final ConfigKey<Integer> CLEARLAG_DELAY_SECONDS = ConfigKeyTypes.integerKey("clear-lag.delay-seconds", 300);

    public static final ConfigKey<String> CHAT_FORMAT = ConfigKeyTypes.stringKey("chat-format", null);
    public static final ConfigKey<String> TABLIST_FORMAT = ConfigKeyTypes.stringKey("tablist-format", null);
    public static final ConfigKey<String> NAMETAG_FORMAT = ConfigKeyTypes.stringKey("nametag-format", null);
    public static final ConfigKey<String> PLUGINS_MESSAGE = ConfigKeyTypes.stringKey("plugins-message", null);

    public static BukkitConfigurateAdaption getAdaption() {
        return new BukkitConfigurateAdaption(YakoCore.getInstance(), YakoCore.getInstance().getCoreConfig());
    }
}
