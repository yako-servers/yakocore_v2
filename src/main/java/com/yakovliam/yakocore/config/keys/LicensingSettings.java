package com.yakovliam.yakocore.config.keys;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.ConfigKey;
import com.yakovliam.yakocoreapi.config.ConfigKeyTypes;
import com.yakovliam.yakocoreapi.config.adapter.BukkitConfigurateAdaption;

public class LicensingSettings {

    public static final ConfigKey<String> POST_URL = ConfigKeyTypes.stringKey("url", null);
    public static final ConfigKey<String> AUTH_ID = ConfigKeyTypes.stringKey("authId", null);
    public static final ConfigKey<String> AUTH_TOKEN = ConfigKeyTypes.stringKey("authToken", null);

    public static BukkitConfigurateAdaption getAdaption() {
        return new BukkitConfigurateAdaption(YakoCore.getInstance(), YakoCore.getInstance().getLicensingConfig());
    }
}
