package com.yakovliam.yakocore.config;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.Config;

public class RulesConfig extends Config {
    public RulesConfig() {
        super(YakoCore.getInstance(), "rules.yml");
    }
}
