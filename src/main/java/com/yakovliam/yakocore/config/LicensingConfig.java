package com.yakovliam.yakocore.config;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocoreapi.config.Config;

public class LicensingConfig extends Config {

    public LicensingConfig() {
        super(YakoCore.getInstance(), "licensing.yml" );
    }
}
