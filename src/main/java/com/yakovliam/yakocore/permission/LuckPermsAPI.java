package com.yakovliam.yakocore.permission;

import net.luckperms.api.LuckPerms;
import org.bukkit.Bukkit;
import org.bukkit.plugin.RegisteredServiceProvider;

public class LuckPermsAPI {

    private static LuckPerms luckPermsAPI;

    public static LuckPerms getLuckPermsAPI() {
        return luckPermsAPI;
    }

    public LuckPermsAPI() {
        RegisteredServiceProvider<LuckPerms> provider = Bukkit.getServicesManager().getRegistration(LuckPerms.class);
        if (provider != null) {
            luckPermsAPI = provider.getProvider();
        }
    }
}