package com.yakovliam.yakocore.permission;

import net.luckperms.api.model.user.User;

import java.util.UUID;

public interface PermissionAdapter {

    boolean hasMeta(User user, String key);

    boolean hasPermission(UUID uuid, String permissionNode);
}
