package com.yakovliam.yakocore.permission;

import net.luckperms.api.LuckPerms;
import net.luckperms.api.cacheddata.CachedMetaData;
import net.luckperms.api.cacheddata.CachedPermissionData;
import net.luckperms.api.context.ContextManager;
import net.luckperms.api.context.ImmutableContextSet;
import net.luckperms.api.model.user.User;
import net.luckperms.api.query.QueryOptions;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;

import java.util.Objects;
import java.util.UUID;

public class LuckPermsAdapter {

    public static boolean hasMeta(User user, String key) {

        OfflinePlayer player = Bukkit.getPlayer(user.getUniqueId());

        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();

        QueryOptions queryOptions = api.getContextManager().getQueryOptions(player);
        CachedMetaData cachedMetaData = user.getCachedData().getMetaData(queryOptions);

        return cachedMetaData.getMetaValue(key) != null && !Objects.equals(cachedMetaData.getMetaValue(key), "");
    }

    public static boolean hasPermission(UUID uuid, String permission) {
        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();

        User user = api.getUserManager().getUser(uuid);

        ContextManager contextManager = LuckPermsAPI.getLuckPermsAPI().getContextManager();
        ImmutableContextSet contextSet = contextManager.getContext(user).orElseGet(contextManager::getStaticContext);

        CachedPermissionData permissionData = user.getCachedData().getPermissionData(QueryOptions.contextual(contextSet));
        return permissionData.checkPermission(permission).asBoolean();
    }
}
