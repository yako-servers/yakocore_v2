package com.yakovliam.yakocore.storage.impl.mysql;

import com.yakovliam.yakocore.config.keys.StorageSettings;
import com.yakovliam.yakocore.cosmetics.components.ChatColor;
import com.yakovliam.yakocore.cosmetics.components.NameColor;
import com.yakovliam.yakocore.cosmetics.components.Title;
import com.yakovliam.yakocore.storage.AbstractStorage;
import com.yakovliam.yakocore.storage.impl.mysql.connection.MysqlConnectionFactory;
import com.yakovliam.yakocore.user.User;
import org.bukkit.entity.Player;

import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.function.BiConsumer;

import static com.yakovliam.yakocore.config.keys.StorageSettings.*;

public class MysqlStorageImpl extends AbstractStorage {

    private Connection connection;

    public MysqlStorageImpl() {

        try {
            connection = new MysqlConnectionFactory().getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            connection = null;
        }
    }

    private static final String SELECT_USER = "SELECT * FROM " + MYSQL_TABLES_PLAYERS.get(StorageSettings.getAdaption()) + " WHERE uuid = ?";
    private static final String INSERT_USER = "INSERT INTO " + MYSQL_TABLES_PLAYERS.get(StorageSettings.getAdaption()) + " (uuid, joindate) VALUES (?, ?)";

    private static final String GET_CHATCOLOR = "SELECT * FROM " + MYSQL_TABLES_CHATCOLORS.get(StorageSettings.getAdaption()) + " WHERE name = ?";
    private static final String GET_NAMECOLOR = "SELECT * FROM " + MYSQL_TABLES_NAMECOLORS.get(StorageSettings.getAdaption()) + " WHERE name = ?";
    private static final String GET_TITLE = "SELECT * FROM " + MYSQL_TABLES_TITLES.get(StorageSettings.getAdaption()) + " WHERE name = ?";

    private static final String GET_CHATCOLOR_NAMES = "SELECT * FROM " + MYSQL_TABLES_CHATCOLORS.get(StorageSettings.getAdaption());
    private static final String GET_NAMECOLOR_NAMES = "SELECT * FROM " + MYSQL_TABLES_NAMECOLORS.get(StorageSettings.getAdaption());
    private static final String GET_TITLE_NAMES = "SELECT * FROM " + MYSQL_TABLES_TITLES.get(StorageSettings.getAdaption());

    private static final String GET_PLAYER_COUNT = "SELECT COUNT(*) FROM " + MYSQL_TABLES_PLAYERS.get(StorageSettings.getAdaption());

    @Override
    public User getUser(UUID uuid) {
        User user = null;

        try {
            PreparedStatement statement = connection.prepareStatement(SELECT_USER);

            statement.setString(1, uuid.toString());

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = new User(uuid);
                user.setJoinDate(new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").parse(resultSet.getString("joindate")));
            }

        } catch (SQLException | ParseException e) {
            e.printStackTrace();
        }

        return user;
    }

    @Override
    public void insertUser(User user) {
        try {
            PreparedStatement statement = connection.prepareStatement(INSERT_USER);

            statement.setString(1, user.getUUID().toString());
            statement.setString(2, new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy").format(user.getJoinDate()));

            statement.execute();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getChatColorNames() {
        List<String> names = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_CHATCOLOR_NAMES);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                if (!names.contains(name)) names.add(name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return names;
    }

    @Override
    public List<String> getTitleNames() {
        List<String> names = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_TITLE_NAMES);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                if (!names.contains(name)) names.add(name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return names;
    }

    @Override
    public List<String> getNameColorNames() {
        List<String> names = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(GET_NAMECOLOR_NAMES);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                String name = resultSet.getString("name");
                if (!names.contains(name)) names.add(name);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return names;
    }

    @Override
    public ChatColor getChatColor(String name) {
        ChatColor chatColor = null;

        try {
            PreparedStatement statement = connection.prepareStatement(GET_CHATCOLOR);

            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                chatColor = new ChatColor(name, resultSet.getString("color"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return chatColor;
    }

    @Override
    public Title getTitle(String name) {
        Title title = null;

        try {
            PreparedStatement statement = connection.prepareStatement(GET_TITLE);

            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                title = new Title(name, resultSet.getString("beforeNameTitle"), resultSet.getString("afterNameTitle"));
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return title;
    }

    @Override
    public NameColor getNameColor(String name) {
        NameColor nameColor = null;

        try {
            PreparedStatement statement = connection.prepareStatement(GET_NAMECOLOR);

            statement.setString(1, name);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                nameColor = new NameColor(name, resultSet.getString("color"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return nameColor;
    }

    @Override
    public Integer getPlayerCount() {
        int playerCount = 0;

        try {
            PreparedStatement statement = connection.prepareStatement(GET_PLAYER_COUNT);

            ResultSet resultSet = statement.executeQuery();
            resultSet.next(); // move cursor forwards

            playerCount = resultSet.getInt(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return playerCount;
    }

    @Override
    public boolean isPlayerNew(UUID uuid) {
        boolean isNewPlayer = true;
        try {
            PreparedStatement statement = connection.prepareStatement(SELECT_USER);
            statement.setString(1, uuid.toString());

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                isNewPlayer = false;
            }
        } catch (SQLException ignored) {
        }
        return isNewPlayer;
    }
}
