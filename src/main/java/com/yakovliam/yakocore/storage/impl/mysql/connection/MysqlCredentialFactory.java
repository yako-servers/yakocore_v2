package com.yakovliam.yakocore.storage.impl.mysql.connection;

import com.yakovliam.yakocore.config.keys.StorageSettings;
import com.yakovliam.yakocore.storage.sql.CredentialProcessor;
import com.yakovliam.yakocore.storage.sql.SqlCredentials;

public class MysqlCredentialFactory implements CredentialProcessor {

    @Override
    public SqlCredentials build() {
        /* dynamically grab, create, and return */
        String address = StorageSettings.MYSQL_ADDRESS.get(StorageSettings.getAdaption());
        Integer port = StorageSettings.MYSQL_PORT.get(StorageSettings.getAdaption());
        String database = StorageSettings.MYSQL_DATABASE.get(StorageSettings.getAdaption());
        String username = StorageSettings.MYSQL_USERNAME.get(StorageSettings.getAdaption());
        String password = StorageSettings.MYSQL_PASSWORD.get(StorageSettings.getAdaption());

        return new SqlCredentials(address, port, database, username, password);
    }
}
