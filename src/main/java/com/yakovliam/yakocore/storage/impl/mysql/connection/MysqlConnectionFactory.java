package com.yakovliam.yakocore.storage.impl.mysql.connection;

import com.yakovliam.yakocore.storage.sql.SqlCredentials;
import com.yakovliam.yakocore.storage.sql.hikari.HikariConfigFactory;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class MysqlConnectionFactory {

    private static HikariConfig config;
    private static HikariDataSource ds;

    public MysqlConnectionFactory() {
        config = new HikariConfigFactory().build(new MysqlCredentialFactory().build()); // build config by creds

        ds = new HikariDataSource(config);

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() throws SQLException {
        return ds.getConnection();
    }
}
