package com.yakovliam.yakocore.storage;

import com.yakovliam.yakocore.storage.impl.mysql.MysqlStorageImpl;

public class StorageManager {

    private AbstractStorage storage;

    public StorageManager() {
        //TODO assign storage to which ever is enabled
        storage = new MysqlStorageImpl();
    }

    public AbstractStorage getStorage() {
        return storage;
    }

    public void setStorage(AbstractStorage storage) {
        this.storage = storage;
    }
}
