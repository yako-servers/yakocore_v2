package com.yakovliam.yakocore.storage;

import com.yakovliam.yakocore.cosmetics.components.ChatColor;
import com.yakovliam.yakocore.cosmetics.components.NameColor;
import com.yakovliam.yakocore.cosmetics.components.Title;
import com.yakovliam.yakocore.user.User;

import java.util.List;
import java.util.UUID;

public abstract class AbstractStorage {

    public abstract User getUser(UUID uuid);
    public abstract void insertUser(User user);

    public abstract List<String> getChatColorNames();
    public abstract List<String> getTitleNames();
    public abstract List<String> getNameColorNames();

    public abstract ChatColor getChatColor(String name);
    public abstract Title getTitle(String name);
    public abstract NameColor getNameColor(String name);

    public abstract Integer getPlayerCount();
    public abstract boolean isPlayerNew(UUID uuid);
}
