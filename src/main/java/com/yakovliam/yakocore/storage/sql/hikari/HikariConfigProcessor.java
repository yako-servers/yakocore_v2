package com.yakovliam.yakocore.storage.sql.hikari;

import com.yakovliam.yakocore.storage.sql.SqlCredentials;
import com.zaxxer.hikari.HikariConfig;

public interface HikariConfigProcessor {

    abstract HikariConfig build(SqlCredentials credentials);

}
