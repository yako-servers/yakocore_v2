package com.yakovliam.yakocore.storage.sql.hikari;

import com.yakovliam.yakocore.config.keys.StorageSettings;
import com.yakovliam.yakocore.storage.sql.ConnectionProperties;
import com.yakovliam.yakocore.storage.sql.SqlCredentials;
import com.zaxxer.hikari.HikariConfig;

public class HikariConfigFactory implements HikariConfigProcessor {

    @Override
    public HikariConfig build(SqlCredentials credentials) {
        HikariConfig config = new HikariConfig();

        String connectionAddress = ConnectionProperties.MYSQL_ADDRESS.getAddress().replace("%url%", credentials.getAddress()).replace("%port%", credentials.getPort().toString()).replace("%db%", credentials.getDatabase());
        config.setJdbcUrl(connectionAddress);

        config.setUsername(credentials.getUsername());
        config.setPassword(credentials.getPassword());
        config.setMaximumPoolSize(StorageSettings.HIKARI_MAX_POOL_SIZE.get(StorageSettings.getAdaption()));
        config.setIdleTimeout(StorageSettings.HIKARI_SET_IDLE_TIMEOUT.get(StorageSettings.getAdaption()));

        return config;
    }
}
