package com.yakovliam.yakocore.storage.sql;

public enum ConnectionProperties {

    MYSQL_ADDRESS("jdbc:mysql://%url%:%port%/%db%"),

    ;

    private String address;

    ConnectionProperties(String address) {
        this.address = address;
    }

    public String getAddress() {
        return address;
    }
}
