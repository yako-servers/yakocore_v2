package com.yakovliam.yakocore.storage.sql;

public interface CredentialProcessor {

    abstract SqlCredentials build();

}
