package com.yakovliam.yakocore.storage;

public enum StorageType {
    MYSQL,
    YAML
}
