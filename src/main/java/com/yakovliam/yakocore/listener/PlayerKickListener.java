package com.yakovliam.yakocore.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;

public class PlayerKickListener implements Listener {

    // prevents disconnect.spam (really, bukkit? I am not a fan...)
    @EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
    public void onPlayerKick(PlayerKickEvent event) {
        if (event.getReason().equals("disconnect.spam")) {
            event.setCancelled(true);
        }
    }
}
