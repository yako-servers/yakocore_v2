package com.yakovliam.yakocore.listener;

import com.yakovliam.yakocore.Messages;
import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.user.UserManager;
import com.yakovliam.yakocore.utility.SpawnUtility;
import com.yakovliam.yakocoreapi.chat.Message;
import de.myzelyam.api.vanish.VanishAPI;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.UUID;

import static com.yakovliam.yakocore.Messages.*;
import static com.yakovliam.yakocore.config.keys.ConfigSettings.SPAWN_TELEPORT_ON_JOIN;

public class PlayerJoinQuitListener implements Listener {

    @EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
    public void onPlayerJoin(PlayerJoinEvent event) {
        // cancel the chat message
        event.setJoinMessage(null);

        // check to see if the player is new
        UUID uuid = event.getPlayer().getUniqueId();
        Player player = event.getPlayer();


        boolean isPlayerNew = SpawnUtility.isPlayerNew(uuid);

        // load the player in cache (or create if not yet)
        UserManager.get(uuid);

        if (isPlayerNew) {

            int playerCount = SpawnUtility.getPlayerCount();

            // broadcast join message to all players
            String message = NEW_JOIN_MESSAGE.getMessage();
            message = PlaceholderAPI.setPlaceholders(player, message);
            new Message("temp-new-join-message", message).broadcast("%player%", player.getDisplayName(), "%count%", Integer.toString(playerCount));

        } else {
            // for players who are NOT new

            // check to see if the player is vanished or not
            if (!VanishAPI.isInvisible(player)) { // if the player is NOT vanished, broadcast the join message
                // broadcast join message to all players
                String message = JOIN_MESSAGE.getMessage();
                message = PlaceholderAPI.setPlaceholders(player, message);
                new Message("temp-join-message", message).broadcast("%player%", player.getDisplayName());
            }
        }

        // teleport to spawn on join (if applicable)
        if (SPAWN_TELEPORT_ON_JOIN.get(ConfigSettings.getAdaption())) {
            SpawnUtility.teleportToSpawn(player);
        }
    }

    @EventHandler(ignoreCancelled = true)
    public void onPlayerQuit(PlayerQuitEvent event) {
        // cancel the chat message
        event.setQuitMessage(null);

        Player player = event.getPlayer();

        if (!VanishAPI.isInvisible(player)) {  // if the player is NOT vanished, broadcast the leave message
            // broadcast join message to all players
            String message = LEAVE_MESSAGE.getMessage();
            message = PlaceholderAPI.setPlaceholders(player, message);
            new Message("temp-leave-message", message).broadcast("%player%", player.getDisplayName());

            YakoCore.getInstance().getLogger().info(player.getName() + " (" + player.getAddress().getHostName() + ") left the server");
        }

        // invalidate user
        UserManager.invalidate(event.getPlayer().getUniqueId());
    }
}