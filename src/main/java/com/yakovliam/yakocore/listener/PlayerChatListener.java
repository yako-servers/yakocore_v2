package com.yakovliam.yakocore.listener;

import com.yakovliam.yakocore.chat.ChatMiddleWareManager;
import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.user.User;
import com.yakovliam.yakocore.user.UserManager;
import com.yakovliam.yakocore.utility.StringEscapeUtils;
import me.clip.placeholderapi.PlaceholderAPI;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.CHAT_FORMAT;

public class PlayerChatListener implements Listener {

    @EventHandler
    public void onPlayerChat(AsyncPlayerChatEvent event) {
        User user = UserManager.get(event.getPlayer().getUniqueId());

        // get format from config
        String chatFormat = CHAT_FORMAT.get(ConfigSettings.getAdaption());

        // replace placeholders
        chatFormat = PlaceholderAPI.setPlaceholders(event.getPlayer(), chatFormat);

        // color
        chatFormat = ChatColor.translateAlternateColorCodes('&', chatFormat);

        // parse thru middle-wares
        String message = ChatMiddleWareManager.apply(user, event.getMessage());
        // escape message
        message = StringEscapeUtils.escapeSpecial(message);

        // replace message
        chatFormat = chatFormat.replace("%message%", message);
        // escape format
        chatFormat = StringEscapeUtils.escapeSpecial(chatFormat);

        event.setFormat(chatFormat);
    }
}
