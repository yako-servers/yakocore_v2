package com.yakovliam.yakocore;

import com.yakovliam.yakocore.config.MessagesConfig;
import com.yakovliam.yakocoreapi.chat.Message;
import org.bukkit.configuration.file.FileConfiguration;

public class Messages {

    // generic
    public static Message CORE_HELP;

    public static Message PLEASE_WAIT = new Message("please-wait", "&7Working...");

    // player join / quit
    public static Message NEW_JOIN_MESSAGE = new Message("core-join-message", "&6New player! Hi, &7%player%&6! &c(#&3%count%&c)");
    public static Message JOIN_MESSAGE = new Message("core-join-message", "%player% &7joined.");
    public static Message LEAVE_MESSAGE = new Message("core-leave-message", "%player% &7left.");

    // spawn
    public static Message SET_SPAWN_HELP = new Message("core-set-spawn-help", "&c/setspawn &7- Sets the spawn. No Arguments");
    public static Message SET_SPAWN = new Message("core-set-spawn", "&bSpawn set to your current location!");
    public static Message CORE_SPAWN = new Message("core-spawn", "&bTeleported to spawn.");

    // cosmetics
    public static Message COSMETICS_NAMECOLOR_SET = new Message("cosmetics-name-color-set", "&7Set your name color to %name%&7!");
    public static Message COSMETICS_CHATCOLOR_SET = new Message("cosmetics-chat-color-set", "&7Set your chat color to %name%&7!");
    public static Message COSMETICS_TITLE_SET = new Message("cosmetics-set-title", "&bYou set your title! It looks like this: &r%name%");
    public static Message COSMETICS_TITLE_CLEARED = new Message("cosmetics-cleared-title", "&bYou cleared your title!");

    // reload
    public static Message RELOADED = new Message("reloaded", "&bSuccessfully reloaded &6YakoCore v" + YakoCore.getInstance().getDescription().getVersion() + "&b!");
    public static Message RELOAD_ERROR = new Message("reload-error", "&rFailed to reload &6YakoCore v" + YakoCore.getInstance().getDescription().getVersion() + "&c!");

    // clear lag
    public static Message CLEARLAG_LEFT = new Message("clearlag-left", "%prefix%&cClearing dropped items in &6%left% &c%format%!");
    public static Message CLEARLAG_CLEARED = new Message("clearlag-cleared", "%prefix%&bCleared dropped items!");

    // broadcast
    public static Message STARTED_BROADCAST = new Message("started-broadcast", "&aStarted broadcasting!");
    public static Message STOPPED_BROADCAST = new Message("stopped-broadcast", "&aStopped broadcasting!");

    // rules
    public static Message RULES_INCORRECT_ARGS = new Message("rules-incorrect-args", "&3/rules &7<page name>\n"
            + "&7Available:\n");
    public static Message RULES_PAGE = new Message("rules-page", "&3- &7%name%");
    public static Message RULES_NOT_FOUND = new Message("rules-not-found", "&cThat page was not found!");

    public Messages() {
        // load messages from config
        MessagesConfig messagesConfig = YakoCore.getInstance().getMessagesConfig();
        FileConfiguration config = messagesConfig.getConfig();

        // CORE / SERVER
        CORE_HELP = new Message(config.getString("CORE_HELP.ident"), config.getString("CORE_HELP.default"));
        PLEASE_WAIT = new Message(config.getString("PLEASE_WAIT.ident"), config.getString("PLEASE_WAIT.default"));

        // JOIN / QUIT
        NEW_JOIN_MESSAGE = new Message(config.getString("NEW_JOIN_MESSAGE.ident"), config.getString("NEW_JOIN_MESSAGE.default"));
        JOIN_MESSAGE = new Message(config.getString("JOIN_MESSAGE.ident"), config.getString("JOIN_MESSAGE.default"));
        LEAVE_MESSAGE = new Message(config.getString("LEAVE_MESSAGE.ident"), config.getString("LEAVE_MESSAGE.default"));

        // SPAWN
        SET_SPAWN_HELP = new Message(config.getString("SET_SPAWN_HELP.ident"), config.getString("SET_SPAWN_HELP.default"));
        SET_SPAWN = new Message(config.getString("SET_SPAWN.ident"), config.getString("SET_SPAWN.default"));

        // COSMETICS
        COSMETICS_NAMECOLOR_SET = new Message(config.getString("COSMETICS_NAMECOLOR_SET.ident"), config.getString("COSMETICS_NAMECOLOR_SET.default"));
        COSMETICS_CHATCOLOR_SET = new Message(config.getString("COSMETICS_CHATCOLOR_SET.ident"), config.getString("COSMETICS_CHATCOLOR_SET.default"));
        COSMETICS_TITLE_SET = new Message(config.getString("COSMETICS_TITLE_SET.ident"), config.getString("COSMETICS_TITLE_SET.default"));
        COSMETICS_TITLE_CLEARED = new Message(config.getString("COSMETICS_TITLE_CLEARED.ident"), config.getString("COSMETICS_TITLE_CLEARED.default"));

        // ADMIN
        CLEARLAG_LEFT = new Message(config.getString("CLEARLAG_LEFT.ident"), config.getString("CLEARLAG_LEFT.default"));
        CLEARLAG_CLEARED = new Message(config.getString("CLEARLAG_CLEARED.ident"), config.getString("CLEARLAG_CLEARED.default"));

        // RULES
        RULES_INCORRECT_ARGS = new Message(config.getString("RULES_INCORRECT_ARGS.ident"), config.getString("RULES_INCORRECT_ARGS.default"));
        RULES_PAGE = new Message(config.getString("RULES_PAGE.ident"), config.getString("RULES_PAGE.default"));
        RULES_NOT_FOUND = new Message(config.getString("RULES_NOT_FOUND.ident"), config.getString("RULES_NOT_FOUND.default"));
    }
}
