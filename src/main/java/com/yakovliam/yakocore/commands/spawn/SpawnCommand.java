package com.yakovliam.yakocore.commands.spawn;

import com.yakovliam.yakocore.utility.SpawnUtility;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.NoConsole;
import com.yakovliam.yakocoreapi.command.addons.Permissible;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.yakovliam.yakocore.Messages.CORE_SPAWN;

@NoConsole
@Permissible("yakocore.spawn")
public class SpawnCommand extends YakoCommand {

    public SpawnCommand() {
        super("spawn");
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        // function
        SpawnUtility.teleportToSpawn((Player) sender);
        CORE_SPAWN.msg(sender);
    }
}
