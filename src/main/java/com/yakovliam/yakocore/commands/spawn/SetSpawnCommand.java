package com.yakovliam.yakocore.commands.spawn;


import com.yakovliam.yakocore.utility.SpawnUtility;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.NoConsole;
import com.yakovliam.yakocoreapi.command.addons.Permissible;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static com.yakovliam.yakocore.Messages.SET_SPAWN;
import static com.yakovliam.yakocore.Messages.SET_SPAWN_HELP;

@NoConsole
@Permissible("yakocore.admin.setspawn")
public class SetSpawnCommand extends YakoCommand {

    public SetSpawnCommand() {
        super("setspawn");
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (args.length != 0) {
            SET_SPAWN_HELP.msg(sender);
            return;
        }

        SpawnUtility.setSpawnLocation((Player) sender);
        SET_SPAWN.msg(sender);
    }
}
