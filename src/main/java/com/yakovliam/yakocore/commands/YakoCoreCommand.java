package com.yakovliam.yakocore.commands;

import com.yakovliam.yakocore.commands.subcommand.ReloadSubCommand;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.NoConsole;
import org.bukkit.command.CommandSender;

import java.util.Collections;

import static com.yakovliam.yakocore.Messages.CORE_HELP;

@NoConsole
public class YakoCoreCommand extends YakoCommand {

    public YakoCoreCommand() {
        super("yakocore", "Core command", Collections.singletonList("core"));

        addSubCommands(
                new ReloadSubCommand()
        );

    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        CORE_HELP.msg(sender);
    }
}
