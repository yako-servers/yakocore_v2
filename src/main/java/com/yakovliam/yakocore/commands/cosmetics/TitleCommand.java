package com.yakovliam.yakocore.commands.cosmetics;


import com.yakovliam.yakocore.gui.cosmetics.TitleGui;
import com.yakovliam.yakocore.user.User;
import com.yakovliam.yakocore.user.UserManager;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.NoConsole;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Arrays;

import static com.yakovliam.yakocore.Messages.CORE_HELP;
import static com.yakovliam.yakocore.Messages.PLEASE_WAIT;

@NoConsole
public class TitleCommand extends YakoCommand {

    public TitleCommand() {
        super("title", Arrays.asList("titles", "prefix", "suffix", "prefixes", "suffixes"));
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (args.length != 0) {
            CORE_HELP.msg(sender);
            return;
        }

        Player player = (Player) sender;

        // working message
        PLEASE_WAIT.msg(player);

        User user = UserManager.get(player.getUniqueId());

        // open the gui
        new TitleGui(user, 1).open(player);
    }
}
