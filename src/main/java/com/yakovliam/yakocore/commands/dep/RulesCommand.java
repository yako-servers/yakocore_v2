package com.yakovliam.yakocore.commands.dep;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.config.RulesConfig;
import com.yakovliam.yakocoreapi.chat.Message;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.NoConsole;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.yakovliam.yakocore.Messages.*;

@NoConsole
public class RulesCommand extends YakoCommand {

    public RulesCommand() {
        super("rules", Arrays.asList("rule", "whatnottodo"));
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {

        RulesConfig config = YakoCore.getInstance().getRulesConfig();

        Set<String> availablePages = config.getConfig().getConfigurationSection("pages").getKeys(false);

        if (args.length != 1) {
            // no args, cancel
            RULES_INCORRECT_ARGS.msg(sender);

            // display available
            availablePages.forEach(page -> {
                RULES_PAGE.msg(sender, "%name%", page);
            });

            return;
        }

        /* display based on args */

        // does it exist?
        if (!config.getConfig().getConfigurationSection("pages").contains(args[0])) {
            RULES_NOT_FOUND.msg(sender);
            return;
        }

        // get strings and send (based on page)
        List<String> lines = config.getConfig().getStringList("pages." + args[0]);
        lines.forEach(line -> {
            new Message("temp-rule", line).msg(sender, "%key%", args[0]);
        });
    }
}
