package com.yakovliam.yakocore.commands.broadcast;

import com.yakovliam.yakocore.components.TaskComponentManager;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.Permissible;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

import static com.yakovliam.yakocore.Messages.CORE_HELP;

@Permissible("yakocore.admin.broadcast")
public class BroadcastCommand extends YakoCommand {

    public BroadcastCommand() {
        super("broadcast", Arrays.asList("bc", "bcast"));
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {

        if (args.length < 1) {

            CORE_HELP.msg(sender);
            return;
        }

        // create message from args
        String message = ChatColor.translateAlternateColorCodes('&', String.join(" ", args).trim());

        TaskComponentManager.getBroadcastComponent().broadcast(message);
    }
}
