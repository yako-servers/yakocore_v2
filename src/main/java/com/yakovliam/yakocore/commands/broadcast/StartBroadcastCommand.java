package com.yakovliam.yakocore.commands.broadcast;

import com.yakovliam.yakocore.components.TaskComponent;
import com.yakovliam.yakocore.components.TaskComponentManager;
import com.yakovliam.yakocore.components.broadcast.AutoBroadcastComponent;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.Permissible;
import org.bukkit.command.CommandSender;

import java.util.Arrays;
import java.util.stream.Collectors;

import static com.yakovliam.yakocore.Messages.CORE_HELP;
import static com.yakovliam.yakocore.Messages.STARTED_BROADCAST;

@Permissible("yakocore.admin.startbroadcast")
public class StartBroadcastCommand extends YakoCommand {

    public StartBroadcastCommand() {
        super("startbroadcast", Arrays.asList("startbc", "startbcast"));
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {

        if (args.length != 0) {
            CORE_HELP.msg(sender);
            return;
        }

        // if autoBroadcast exists
        if (TaskComponentManager.toggleableTaskComponents.stream().anyMatch(feature -> feature instanceof AutoBroadcastComponent)) {
            TaskComponent autoBroadcast = TaskComponentManager.toggleableTaskComponents.stream().filter(feature -> feature instanceof AutoBroadcastComponent).collect(Collectors.toList()).get(0);
            if (!autoBroadcast.isEnabled()) {
                // start
                autoBroadcast.startTask();
            }
        }

        STARTED_BROADCAST.msg(sender);
    }
}
