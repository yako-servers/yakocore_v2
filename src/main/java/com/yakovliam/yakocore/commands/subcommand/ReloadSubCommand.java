package com.yakovliam.yakocore.commands.subcommand;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.components.TaskComponentManager;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import com.yakovliam.yakocoreapi.command.addons.Permissible;
import org.bukkit.command.CommandSender;

import static com.yakovliam.yakocore.Messages.RELOADED;
import static com.yakovliam.yakocore.Messages.RELOAD_ERROR;

@Permissible("yakocore.admin.reload")
public class ReloadSubCommand extends YakoCommand {

    public ReloadSubCommand() {
        super("reload");
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {

        try {
            // try to reload features (configs, then components)
            YakoCore.getInstance().getCoreConfig().load();
            YakoCore.getInstance().getMessagesConfig().load();
            TaskComponentManager.reloadFeatures(); // task components
        } catch (Exception e) {
            e.printStackTrace();
            RELOAD_ERROR.msg(sender);
            return;
        }

        RELOADED.msg(sender);
    }
}
