package com.yakovliam.yakocore.commands.blocking;

import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocoreapi.chat.Message;
import com.yakovliam.yakocoreapi.command.YakoCommand;
import org.bukkit.command.CommandSender;

import java.util.Arrays;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.PLUGINS_MESSAGE;

public class PluginsCommand extends YakoCommand {

    public PluginsCommand() {
        super("plugins", Arrays.asList("pl", "about", "icanhasbukkit", "bukkit:about", "bukkit:plugins"));
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        // show the player the message
        Message pluginsMessage = new Message("temp", PLUGINS_MESSAGE.get(ConfigSettings.getAdaption()));
        pluginsMessage.msg(sender);
    }
}
