package com.yakovliam.yakocore.cosmetics;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.cosmetics.components.NameColor;
import org.bukkit.Bukkit;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class NameColorManager {

    private LinkedList<String> nameColorNames = new LinkedList<>();

    private LoadingCache<String, NameColor> nameColorCache = Caffeine.newBuilder()
            .expireAfterAccess(10, TimeUnit.MINUTES)
            .build(key -> YakoCore.getInstance().getStorageManager().getStorage().getNameColor(key));

    public NameColorManager() {

        // on init pre-populate all names for caching later
        Bukkit.getScheduler().runTaskAsynchronously(YakoCore.getInstance(), () -> {
           nameColorNames.addAll(YakoCore.getInstance().getStorageManager().getStorage().getNameColorNames());
        });
    }

    public LinkedList<String> getNameColorNames() {
        return nameColorNames;
    }

    public NameColor get(String name) {
        return nameColorCache.get(name);
    }

    public void invalidate(String name) {
        nameColorCache.invalidate(name);
    }
}