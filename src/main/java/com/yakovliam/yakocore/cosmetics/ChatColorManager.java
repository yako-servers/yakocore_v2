package com.yakovliam.yakocore.cosmetics;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.cosmetics.components.ChatColor;
import org.bukkit.Bukkit;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class ChatColorManager {

    private LinkedList<String> chatColorNames = new LinkedList<>();

    private LoadingCache<String, ChatColor> chatColorCache = Caffeine.newBuilder()
            .expireAfterAccess(10, TimeUnit.MINUTES)
            .build(key -> YakoCore.getInstance().getStorageManager().getStorage().getChatColor(key));

    public ChatColorManager() {

        // on init pre-populate all names for caching later
        Bukkit.getScheduler().runTaskAsynchronously(YakoCore.getInstance(), () -> {
            chatColorNames.addAll(YakoCore.getInstance().getStorageManager().getStorage().getChatColorNames());
        });
    }

    public LinkedList<String> getChatColorNames() {
        return chatColorNames;
    }

    public ChatColor get(String name) {
        return chatColorCache.get(name);
    }

    public void invalidate(String name) {
        chatColorCache.invalidate(name);
    }
}