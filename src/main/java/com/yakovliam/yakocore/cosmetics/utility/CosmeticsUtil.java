package com.yakovliam.yakocore.cosmetics.utility;

import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.cosmetics.components.ChatColor;
import com.yakovliam.yakocore.cosmetics.components.NameColor;
import com.yakovliam.yakocore.cosmetics.components.Title;
import com.yakovliam.yakocore.permission.LuckPermsAPI;
import com.yakovliam.yakocore.permission.LuckPermsAdapter;
import net.luckperms.api.LuckPerms;
import net.luckperms.api.cacheddata.CachedMetaData;
import net.luckperms.api.node.Node;
import net.luckperms.api.query.QueryOptions;
import org.bukkit.Bukkit;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

public class CosmeticsUtil {

    public static String cosmeticFormat = "yakocore.cosmetics";
    public static String cosmeticMetaFormat = "meta.core_current";

    // EXAMPLE HERE, DON'T EVER UNCOMMENT THIS
//    public static User compileCosmetics(User user) throws NullPointerException {
//
//        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
//        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);
//        Optional<QueryOptions> queryOptions = api.getContextManager().getQueryOptions(lpUser);
//        CachedMetaData metaData = lpUser.getCachedData().getMetaData(queryOptions.get());
//
//        // do they have meta?
//        boolean hasPrefixMeta = LuckPermsUtil.hasMeta(lpUser, "core_currentPrefix");
//        boolean hasNameColorMeta = LuckPermsUtil.hasMeta(lpUser, "core_currentNameColor");
//        boolean hasSuffixMeta = LuckPermsUtil.hasMeta(lpUser, "core_currentSuffix");
//        boolean hasChatColorMeta = LuckPermsUtil.hasMeta(lpUser, "core_currentChatColor");
//
//        if (hasPrefixMeta) {
//            // set their prefix to the meta
//            currentPrefix = metaData.getMetaValue("core_currentPrefix");
//        } else {
//            // they don't, so create the meta (and set to null)
//            MetaNode node = MetaNode.builder("core_currentPrefix", "").build();
//            lpUser.getNodes().add(node);
//        }
//    }

    public static String getCosmeticMetaKey(String cosmeticType, String cosmeticName) {
        return cosmeticMetaFormat + cosmeticType + "." + cosmeticName;
    }

    public static void updateMeta(UUID uuid, String oldMetaKey, String newMetaKey) {

        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);

        Node newNode = Node.builder(newMetaKey).build(); // new node
        Node oldNode = Node.builder(oldMetaKey).build();

        lpUser.data().remove(oldNode);
        lpUser.data().add(newNode);

        // save changes
        api.getUserManager().saveUser(lpUser);
    }

    public static void removeMeta(UUID uuid, String metaKey) {

        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);

        Node newNode = Node.builder(metaKey).build(); // new node

        lpUser.data().remove(newNode);

        // save changes
        api.getUserManager().saveUser(lpUser);
    }

    public static void addMeta(UUID uuid, String newMetaKey) {
        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);

        Node newNode = Node.builder(newMetaKey).build(); // new node

        lpUser.data().add(newNode);

        // save changes
        api.getUserManager().saveUser(lpUser);
    }

    public static void removeCosmeticMeta(UUID uuid, String cosmeticType, String cosmeticName) {
        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);

        // build new key
        String key = cosmeticMetaFormat + cosmeticType + "." + cosmeticName;

        Node oldNode = Node.builder(key).build(); // new node

        lpUser.data().remove(oldNode);

        // save changes
        api.getUserManager().saveUser(lpUser);
    }

    public static Title getCurrentTitle(UUID uuid) {
        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);
        Optional<QueryOptions> queryOptions = api.getContextManager().getQueryOptions(lpUser);
        CachedMetaData metaData = lpUser.getCachedData().getMetaData(queryOptions.get());

        boolean hasMeta = LuckPermsAdapter.hasMeta(lpUser, "core_currentTitle");

        if (!hasMeta) return null;

        // get the meta
        String currentCosmeticName = metaData.getMetaValue("core_currentTitle");

        if (currentCosmeticName == null)
            return null;

        // get from title manager
        return YakoCore.getInstance().getTitleManager().get(currentCosmeticName);
    }

    public static NameColor getCurrentNameColor(UUID uuid) {
        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);
        Optional<QueryOptions> queryOptions = api.getContextManager().getQueryOptions(lpUser);
        CachedMetaData metaData = lpUser.getCachedData().getMetaData(queryOptions.get());

        boolean hasMeta = LuckPermsAdapter.hasMeta(lpUser, "core_currentNameColor");

        if (!hasMeta) return null;

        // get the meta
        String currentCosmeticName = metaData.getMetaValue("core_currentNameColor");

        if (currentCosmeticName == null)
            return null;

        // get from title manager
        return YakoCore.getInstance().getNameColorManager().get(currentCosmeticName);
    }

    public static ChatColor getCurrentChatColor(UUID uuid) {
        LuckPerms api = LuckPermsAPI.getLuckPermsAPI();
        net.luckperms.api.model.user.User lpUser = api.getUserManager().getUser(uuid);
        Optional<QueryOptions> queryOptions = api.getContextManager().getQueryOptions(lpUser);
        CachedMetaData metaData = lpUser.getCachedData().getMetaData(queryOptions.get());

        boolean hasMeta = LuckPermsAdapter.hasMeta(lpUser, "core_currentChatColor");

        if (!hasMeta) return null;

        // get the meta
        String currentCosmeticName = metaData.getMetaValue("core_currentChatColor");

        if (currentCosmeticName == null)
            return null;

        // get from title manager
        return YakoCore.getInstance().getChatColorManager().get(currentCosmeticName);
    }

    private static Collection<Node> getPermissionNodes(UUID uuid) {
        QueryOptions queryOptions = LuckPermsAPI.getLuckPermsAPI().getContextManager().getQueryOptions(Bukkit.getPlayer(uuid));

        return Objects.requireNonNull(LuckPermsAPI.getLuckPermsAPI().getUserManager().getUser(uuid)).resolveInheritedNodes(queryOptions);
    }

    public static boolean isValidCosmetic(Node node, String cosmeticType) {
        if (!node.getValue()) return false;


        // format
        int lastIndex = node.getKey().lastIndexOf(".");

        if (lastIndex == -1) return false;

        // yakocore.cosmetics.<TYPE>.<NAME>
        String beforeCosmeticName = node.getKey().substring(0, lastIndex);

        return beforeCosmeticName.equalsIgnoreCase(cosmeticFormat + "." + cosmeticType);
    }

    private static String getCosmeticNameFromNode(Node node) {
        int lastIndex = node.getKey().lastIndexOf(".");

        if (lastIndex == -1) return null;

        try {
            return node.getKey().substring(lastIndex + 1);
        } catch (Exception e) {
            return null;
        }
    }

    public static Collection<Title> getTitles(UUID uuid) {

//        Collection<Node> nodes = getPermissionNodes(uuid)
//                .stream().filter(node -> isValidCosmetic(node, "title")).collect(Collectors.toList());
//
//        Collection<Title> titles = new ArrayList<>();
//
//        nodes.forEach(node -> {
//            String name = getCosmeticNameFromNode(node);
//            // get the cosmetic by the name
//            if (name != null) {
//                Title title = YakoCore.getTitleManager().get(name);
//                if (title != null) // if the title exists, add it
//                    titles.add(title);
//            }
//        });
        //return titles;

        return YakoCore.getInstance().getTitleManager().getTitleNames().stream().filter(name -> LuckPermsAdapter.hasPermission(uuid, getPermissionNodeFromCosmetic("title", name))).map(name -> YakoCore.getInstance().getTitleManager().get(name)).collect(Collectors.toList());
    }

    public static Collection<NameColor> getNameColors(UUID uuid) {

//        Collection<Node> nodes = getPermissionNodes(uuid).stream().filter(node -> isValidCosmetic(node, "namecolor")).collect(Collectors.toList());
////
//        Collection<NameColor> colors = new ArrayList<>();
//
//        nodes.forEach(node -> {
//            String name = getCosmeticNameFromNode(node);
//            // get the cosmetic by the name
//            if (name != null) {
//                NameColor color = YakoCore.getNameColorManager().get(name);
//                if (color != null) // if the title exists, add it
//                    colors.add(color);
//            }
//        });
//
//        return colors;

        return YakoCore.getInstance().getNameColorManager().getNameColorNames().stream().filter(name -> LuckPermsAdapter.hasPermission(uuid, getPermissionNodeFromCosmetic("namecolor", name))).map(name -> YakoCore.getInstance().getNameColorManager().get(name)).collect(Collectors.toList());

    }

    public static Collection<ChatColor> getChatColors(UUID uuid) {

//        Collection<Node> nodes = getPermissionNodes(uuid).stream().filter(node -> isValidCosmetic(node, "chatcolor")).collect(Collectors.toList());
//
//        Collection<ChatColor> colors = new ArrayList<>();
//
//        nodes.forEach(node -> {
//            String name = getCosmeticNameFromNode(node);
//            // get the cosmetic by the name
//            if (name != null) {
//                ChatColor color = YakoCore.getChatColorManager().get(name);
//                if (color != null) // if the title exists, add it
//                    colors.add(color);
//            }
//        });
//
//        return colors;

        return YakoCore.getInstance().getChatColorManager().getChatColorNames().stream().filter(name -> LuckPermsAdapter.hasPermission(uuid, getPermissionNodeFromCosmetic("chatcolor", name))).map(name -> YakoCore.getInstance().getChatColorManager().get(name)).collect(Collectors.toList());
    }

    public static String getPermissionNodeFromCosmetic(String cosmeticType, String cosmeticName) {
        return "yakocore.cosmetics." + cosmeticType + "." + cosmeticName;
    }
}
