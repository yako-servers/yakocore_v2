package com.yakovliam.yakocore.cosmetics.utility;

import com.yakovliam.yakocore.config.keys.ConfigSettings;
import com.yakovliam.yakocore.cosmetics.components.ChatColor;
import com.yakovliam.yakocore.cosmetics.components.NameColor;
import com.yakovliam.yakocore.cosmetics.components.Title;
import com.yakovliam.yakocore.permission.LuckPermsAdapter;
import com.yakovliam.yakocore.user.User;
import org.bukkit.Bukkit;

import static com.yakovliam.yakocore.config.keys.ConfigSettings.*;

public class ChatUtil {

    public static String getDisplayNameFormat(User user) {
        // get the format
        String displayNameFormat = COSMETICS_DISPLAYNAME_FORMAT.get(ConfigSettings.getAdaption());
        String defaultNameColor = COSMETICS_DEFAULT_NAMECOLOR.get(ConfigSettings.getAdaption());

        String prefix = "";
        String suffix = "";
        String nameColor = defaultNameColor;

        // replace elements
        // ----------------------------------------------------------------------------------------------------------------------
        Title currentTitle = CosmeticsUtil.getCurrentTitle(user.getUUID());
        if (currentTitle != null) {
            if (LuckPermsAdapter.hasPermission(user.getUUID(), CosmeticsUtil.getPermissionNodeFromCosmetic("title", currentTitle.getName()))) {
                if (currentTitle.getBeforeNameTitle() != null)
                    prefix = currentTitle.getBeforeNameTitle();

                if (currentTitle.getAfterNameTitle() != null)
                    suffix = currentTitle.getAfterNameTitle();

            } else {
                // remove, since they don't have permission
                CosmeticsUtil.removeCosmeticMeta(user.getUUID(), "Title", currentTitle.getName());
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------
        NameColor currentNameColor = CosmeticsUtil.getCurrentNameColor(user.getUUID());
        if (currentNameColor != null) {
            if (LuckPermsAdapter.hasPermission(user.getUUID(), CosmeticsUtil.getPermissionNodeFromCosmetic("namecolor", currentNameColor.getName()))) {
                nameColor = currentNameColor.getColor();

            } else {
                // remove, since they don't have permission
                CosmeticsUtil.removeCosmeticMeta(user.getUUID(), "NameColor", currentNameColor.getName());
            }
        }

        // ----------------------------------------------------------------------------------------------------------------------
        displayNameFormat = displayNameFormat.replace("%username%", org.bukkit.ChatColor.stripColor(Bukkit.getPlayer(user.getUUID()).getName()));
        displayNameFormat = displayNameFormat.replace("%prefix%", prefix);
        displayNameFormat = displayNameFormat.replace("%suffix%", suffix);
        displayNameFormat = displayNameFormat.replace("%namecolor%", nameColor);

        return org.bukkit.ChatColor.translateAlternateColorCodes('&', displayNameFormat);
    }

    public static String getChatColor(User user) {

        String chatColor = COSMETICS_DEFAULT_CHATCOLOR.get(ConfigSettings.getAdaption());

        // replace elements
        ChatColor currentChatColor = CosmeticsUtil.getCurrentChatColor(user.getUUID());

        if (currentChatColor != null) {
            if (LuckPermsAdapter.hasPermission(user.getUUID(), CosmeticsUtil.getPermissionNodeFromCosmetic("chatcolor", currentChatColor.getName()))) {
                chatColor = currentChatColor.getColor();
            } else {
                // remove, since they don't have permission
                CosmeticsUtil.removeCosmeticMeta(user.getUUID(), "ChatColor", currentChatColor.getName());
            }
        }

        return chatColor;
    }
}
