package com.yakovliam.yakocore.cosmetics;

import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.benmanes.caffeine.cache.LoadingCache;
import com.yakovliam.yakocore.YakoCore;
import com.yakovliam.yakocore.cosmetics.components.Title;
import org.bukkit.Bukkit;

import java.util.LinkedList;
import java.util.concurrent.TimeUnit;

public class TitleManager {

    private LinkedList<String> titleNames = new LinkedList<>();

    private LoadingCache<String, Title> titleCache = Caffeine.newBuilder()
            .expireAfterAccess(10, TimeUnit.MINUTES)
            .build(key -> YakoCore.getInstance().getStorageManager().getStorage().getTitle(key));

    public TitleManager() {
        // on init pre-populate all names for caching later
        Bukkit.getScheduler().runTaskAsynchronously(YakoCore.getInstance(), () -> {
            titleNames.addAll(YakoCore.getInstance().getStorageManager().getStorage().getTitleNames());
        });
    }

    public LinkedList<String> getTitleNames() {
        return titleNames;
    }

    public Title get(String name) {
        return titleCache.get(name);
    }

    public void invalidate(String name) {
        titleCache.invalidate(name);
    }
}
