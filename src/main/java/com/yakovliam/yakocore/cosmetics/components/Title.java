package com.yakovliam.yakocore.cosmetics.components;

public class Title {

    private String name;
    private String beforeNameTitle;
    private String afterNameTitle;

    public Title(String name, String beforeNameTitle, String afterNameTitle) {
        this.name = name;
        this.beforeNameTitle = beforeNameTitle;
        this.afterNameTitle = afterNameTitle;
    }

    public Title(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getBeforeNameTitle() {
        return beforeNameTitle;
    }

    public String getAfterNameTitle() {
        return afterNameTitle;
    }
}
